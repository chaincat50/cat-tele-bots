package org.butch;

import org.butch.encryption.CryptoUtils;

import javax.crypto.SecretKey;

public class KeyGenerator {
    public static void main(String[] args) throws Exception {
        final SecretKey key = CryptoUtils.getAESKey();
        System.out.println(CryptoUtils.keyToString(key));
    }
}
