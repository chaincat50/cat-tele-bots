package org.butch.services;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.butch.bots.base.AbstractBot;
import org.butch.bots.feedback.master.FeedbackMasterBot;
import org.butch.bots.id.IdBot;
import org.butch.bots.poll.PollBot;
import org.butch.bots.reminder.ReminderBot;
import org.butch.bots.test.TestBot;
import org.butch.config.BotsConfig;
import org.butch.encryption.CryptoUtils;
import org.butch.repositories.FeedbackStorageImpl;
import org.butch.repositories.ReminderStorageImpl;
import org.butch.repositories.VersionStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

@SuppressWarnings("unused")
@Service
public class BotsService {
    private final Logger LOGGER = LoggerFactory.getLogger(BotsService.class);

    private final BotsConfig botsConfig;
    private final List<BotRunnable> bots = new ArrayList<>();

    // Don't use those directly
    private DataSource pDataSource;
    private VersionStorage pVersionStorage;

    @Scheduled(fixedRate = 1000L)
    public void timer() {
        final long now = System.currentTimeMillis();

        for (BotRunnable runnable : bots) {
            try {
                runnable.bot.onTimer(now);
                if (runnable.ex != null) {
                    LOGGER.info(runnable.bot.getBotUsername() + " recovered");
                    runnable.ex = null;
                }
            } catch (Throwable ex) {
                if (runnable.ex == null) {
                    LOGGER.error(runnable.bot.getBotUsername() + " exception", ex);
                }
                runnable.ex = ex;
            }
        }
    }

    @Autowired
    public BotsService(BotsConfig botsConfig) {
        this.botsConfig = botsConfig;
    }

    @PostConstruct
    public void init() throws TelegramApiException {
        final String dbUrl = System.getenv("JDBC_DATABASE_URL");

        final TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);

        tryInitBot(botsConfig.getPoll(), "poll", botsApi,
                () -> new PollBot(System.getenv("POLLBOT_ID"))
        );
        tryInitBot(botsConfig.getReminder(), "reminder", botsApi,
                () -> {
                    final DataSource dataSource = getDataSource(dbUrl);
                    final ReminderStorageImpl storage = new ReminderStorageImpl(dataSource);
                    return new ReminderBot(System.getenv("REMINDERBOT_ID"), storage);
                }
        );
        tryInitBot(botsConfig.getFeedback(), "feedback", botsApi,
                () -> {
                    final String token = System.getenv("FEEDBACKMASTERBOT_ID");
                    final String key = System.getenv("FEEDBACKMASTERBOT_KEY");
                    final DataSource dataSource = getDataSource(dbUrl);
                    final VersionStorage versionStorage = getVersionStorage(dataSource);
                    final FeedbackStorageImpl storage = new FeedbackStorageImpl(dataSource, versionStorage);
                    return new FeedbackMasterBot(botsApi, token, storage, CryptoUtils.keyFromString(key));
                }
        );
        tryInitBot(botsConfig.getId(), "id", botsApi,
                () -> new IdBot(System.getenv("IDBOT_ID"))
        );
        tryInitBot(botsConfig.getTest(), "test", botsApi,
                () -> new TestBot(System.getenv("TESTBOT_ID"))
        );
    }

    private <TBot extends AbstractBot> TBot tryInitBot(boolean enabled,
                                                       String name,
                                                       TelegramBotsApi botsApi,
                                                       Callable<TBot> producer) {
        if (enabled) {
            LOGGER.info("Starting " + name + " bot.");
            try {
                final TBot ret = producer.call();
                initBot(botsApi, ret);
                return ret;
            } catch (Throwable ex) {
                LOGGER.error("Could not init bot " + name, ex);
            }
        } else {
            LOGGER.info("Skipping " + name + " bot.");
        }
        return null;
    }

    private void initBot(TelegramBotsApi botsApi, AbstractBot bot) throws TelegramApiException {
        bots.add(new BotRunnable(bot));
        botsApi.registerBot(bot);
    }

    private DataSource getDataSource(String dbUrl) {
        if (pDataSource == null) {
            final HikariConfig config = new HikariConfig();
            config.setJdbcUrl(dbUrl);
            pDataSource = new HikariDataSource(config);
        }
        return pDataSource;
    }

    private VersionStorage getVersionStorage(DataSource dataSource) {
        if (pVersionStorage == null) {
            pVersionStorage = new VersionStorage(dataSource);
            pVersionStorage.init();
        }
        return pVersionStorage;
    }

    private static class BotRunnable {
        private final AbstractBot bot;
        private Throwable ex = null;

        private BotRunnable(AbstractBot bot) {
            this.bot = bot;
        }

        public void onTimer(long now) {
            bot.onTimer(now);
        }
    }
}
