package org.butch.repositories;

import org.butch.bots.reminder.repository.Chat;
import org.butch.bots.reminder.repository.Storage;
import org.butch.bots.reminder.repository.Sub;
import org.butch.bots.reminder.repository.SubType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReminderStorageImpl implements Storage {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private final DataSource dataSource;

    public ReminderStorageImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void init() {
        try (Connection connection = dataSource.getConnection()) {
            try (Statement stmt = connection.createStatement()) {
                stmt.executeUpdate("" +
                        "CREATE TABLE IF NOT EXISTS reminder_subs ( " +
                        "id SERIAL PRIMARY KEY, " +
                        "chat_id VARCHAR(1024) NOT NULL, " +
                        "message TEXT, " +
                        "time BIGINT, " +
                        "type INTEGER" +
                        ");"
                );
                stmt.executeUpdate("" +
                        "ALTER TABLE reminder_subs " +
                        "ADD COLUMN IF NOT EXISTS " +
                        "last_execute_time INTEGER;"
                );
                stmt.executeUpdate("" +
                        "ALTER TABLE reminder_subs " +
                        "ALTER COLUMN " +
                        "last_execute_time TYPE BIGINT;"
                );
                stmt.executeUpdate("" +
                        "CREATE TABLE IF NOT EXISTS reminder_chats ( " +
                        "chat_id VARCHAR(255) PRIMARY KEY, " +
                        "time_zone VARCHAR(255) NOT NULL" +
                        ");"
                );
            }
        } catch (SQLException ex) {
            LOGGER.error("init", ex);
        }
    }

    @Override
    public boolean insertChat(Chat chat) {
        LOGGER.debug("insertChat: " + chat);
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "INSERT INTO reminder_chats (chat_id, time_zone) " +
                    "VALUES (?, ?);")) {
                stmt.setString(1, chat.getChatId());
                stmt.setString(2, chat.getTimeZone());
                return stmt.executeUpdate() == 1;
            }
        } catch (SQLException ex) {
            LOGGER.error("insertChat: " + chat, ex);
            return false;
        }
    }

    @Override
    public boolean updateChat(Chat chat) {
        LOGGER.debug("updateChat: " + chat);
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "UPDATE reminder_chats SET " +
                    "time_zone = ? " +
                    "WHERE chat_id = ?;")) {
                stmt.setString(1, chat.getTimeZone());
                stmt.setString(2, chat.getChatId());
                return stmt.executeUpdate() == 1;
            }
        } catch (SQLException ex) {
            LOGGER.error("updateChat: " + chat, ex);
            return false;
        }
    }

    @Override
    public boolean deleteChat(Chat chat) {
        LOGGER.debug("deleteChat: " + chat);
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "DELETE FROM reminder_chats WHERE chat_id = ?;")) {
                stmt.setString(1, chat.getChatId());
                return stmt.executeUpdate() == 1;
            }
        } catch (SQLException ex) {
            LOGGER.error("deleteChat: " + chat, ex);
            return false;
        }
    }

    @Override
    public List<Chat> queryChats() {
        LOGGER.debug("queryChats");
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "SELECT chat_id, time_zone FROM reminder_chats;")) {
                final ResultSet resultSet = stmt.executeQuery();
                final List<Chat> ret = new ArrayList<>();
                while (resultSet.next()) {
                    final Chat chat = new Chat();
                    chat.setChatId(resultSet.getString(1));
                    chat.setTimeZone(resultSet.getString(2));
                    ret.add(chat);
                }
                return ret;
            }
        } catch (SQLException ex) {
            LOGGER.error("queryChats", ex);
            return Collections.emptyList();
        }
    }

    @Override
    public int insertSub(Sub sub) {
        LOGGER.debug("insertSub: " + sub);
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "INSERT INTO reminder_subs (chat_id, message, time, type, last_execute_time) " +
                    "VALUES (?, ?, ?, ?, ?) " +
                    "RETURNING id;")) {
                stmt.setString(1, sub.getChatId());
                stmt.setString(2, sub.getMessage());
                stmt.setLong(3, sub.getTime());
                stmt.setInt(4, sub.getType().getNumber());
                stmt.setLong(5, sub.getLastExecuteTime());
                final ResultSet resultSet = stmt.executeQuery();
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                }
                LOGGER.error("id was not returned");
                return -1;
            }
        } catch (SQLException ex) {
            LOGGER.error("insertSub: " + sub, ex);
            return -1;
        }
    }

    @Override
    public boolean updateSub(Sub sub) {
        LOGGER.debug("updateSub: " + sub);
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "UPDATE reminder_subs SET " +
                    "last_execute_time = ? " +
                    "WHERE id = ?;")) {
                stmt.setLong(1, sub.getLastExecuteTime());
                stmt.setInt(2, sub.getId());
                return stmt.executeUpdate() == 1;
            }
        } catch (SQLException ex) {
            LOGGER.error("updateSub: " + sub, ex);
            return false;
        }
    }

    @Override
    public boolean deleteSub(Sub sub) {
        LOGGER.debug("deleteSub: " + sub);
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "DELETE FROM reminder_subs WHERE id = ?;")) {
                stmt.setInt(1, sub.getId());
                return stmt.executeUpdate() == 1;
            }
        } catch (SQLException ex) {
            LOGGER.error("deleteSub: " + sub, ex);
            return false;
        }
    }

    @Override
    public List<Sub> querySubs() {
        LOGGER.debug("querySubs");
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "SELECT id, chat_id, message, time, type, last_execute_time FROM reminder_subs;")) {
                final ResultSet resultSet = stmt.executeQuery();
                final List<Sub> ret = new ArrayList<>();
                while (resultSet.next()) {
                    final Sub sub = new Sub();
                    sub.setId(resultSet.getInt(1));
                    sub.setChatId(resultSet.getString(2));
                    sub.setMessage(resultSet.getString(3));
                    sub.setTime(resultSet.getLong(4));
                    sub.setType(SubType.fromNumber(resultSet.getInt(5)));
                    sub.setLastExecuteTime(resultSet.getLong(6));
                    ret.add(sub);
                }
                return ret;
            }
        } catch (SQLException ex) {
            LOGGER.error("querySubs", ex);
            return Collections.emptyList();
        }
    }
}
