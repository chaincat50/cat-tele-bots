package org.butch.repositories;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.*;

public class VersionStorage {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private final DataSource dataSource;

    public VersionStorage(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void init() {
        try (Connection connection = dataSource.getConnection()) {
            try (Statement stmt = connection.createStatement()) {
                stmt.executeUpdate("" +
                        "CREATE TABLE IF NOT EXISTS version ( " +
                        "name VARCHAR(255) NOT NULL PRIMARY KEY, " +
                        "version VARCHAR(255) NOT NULL" +
                        ");"
                );
            }
        } catch (SQLException ex) {
            LOGGER.error("init", ex);
        }
    }

    public String getVersion(String name) {
        LOGGER.info("getVersion: " + name);
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "SELECT version FROM version WHERE name = ?;")) {
                stmt.setString(1, name);
                final ResultSet resultSet = stmt.executeQuery();
                if (resultSet.next()) {
                    return resultSet.getString(1);
                }
                return null;
            }
        } catch (Exception ex) {
            LOGGER.error("getVersion", ex);
            return null;
        }
    }

    public boolean setVersion(String name, String version) {
        LOGGER.info("setVersion: " + name + " = " + version);
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("" +
                    "INSERT INTO version (name, version) VALUES (?, ?) " +
                    "ON CONFLICT (name) DO UPDATE SET version = EXCLUDED.version;")) {
                stmt.setString(1, name);
                stmt.setString(2, version);
                return stmt.executeUpdate() == 1;
            }
        } catch (Exception ex) {
            LOGGER.error("setVersion: " + name + " = " + version, ex);
            return false;
        }
    }
}
