package org.butch.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@SuppressWarnings("unused")
@RestController
@RequestMapping("api/v1/test")
public class TestController {
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String getTest() {
        return "Application is running!";
    }
}
