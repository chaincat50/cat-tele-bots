package org.butch.utils;

public class TimeUtils {
    private TimeUtils() {}

    public static final long MS_IN_SECOND = 1000;
    public static final long MS_IN_MINUTE = 60 * MS_IN_SECOND;
    public static final long MS_IN_HOUR = 60 * MS_IN_MINUTE;
    public static final long MS_IN_DAY = 24 * MS_IN_HOUR;
    public static final long MS_IN_10_MIN = 10 * MS_IN_MINUTE;

    public static final long MAX_TIME = 24 * MS_IN_HOUR;

    public static long getDate(long time) {
        return (time / MS_IN_DAY) * MS_IN_DAY;
    }
}
