package org.butch.utils;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Randomizer {
    private static final String ALPHANUMERIC_STR = "1234567890qwertyuiopasdfghjklzxcvbnm";

    private final Random rnd;

    public Randomizer() throws NoSuchAlgorithmException, NoSuchProviderException {
        this(SecureRandom.getInstance("SHA1PRNG", "SUN"));
    }

    public Randomizer(Random rnd) {
        this.rnd = rnd;
    }

    public int nextInt(int bound) {
        return rnd.nextInt(bound);
    }

    public String nextString(int min, int max) {
        final int size = rnd.nextInt(max - min) + min;
        final StringBuilder builder = new StringBuilder(size);
        for (int i = 0; i < size; ++i) {
            builder.append(ALPHANUMERIC_STR.charAt(rnd.nextInt(ALPHANUMERIC_STR.length())));
        }
        return builder.toString();
    }

    public <T> T getItem(List<T> items) {
        return items.get(rnd.nextInt(items.size()));
    }

    public <T> List<T> getItems(List<T> items, int n) {
        if (items.size() < n)
            throw new IndexOutOfBoundsException("Not enough items");

        final List<T> ret = new LinkedList<>(items);
        while (ret.size() > n) {
            ret.remove(rnd.nextInt(ret.size()));
        }
        return ret;
    }
}
