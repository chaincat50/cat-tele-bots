package org.butch.bots.base.params;

public enum Result {
    EXACT_MATCH,
    PARTIAL_MATCH,
    NO_MATCH
}
