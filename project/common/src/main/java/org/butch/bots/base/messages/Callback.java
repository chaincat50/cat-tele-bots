package org.butch.bots.base.messages;

import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Update;

public class Callback extends Base {
    private final String data;

    public Callback(Update update, String data) {
        super(update);
        this.data = data;
    }

    @Override
    public Type getType() {
        return Type.CALLBACK_QUERY;
    }

    public String getData() {
        return data;
    }

    public CallbackQuery getQuery() {
        return getUpdate().getCallbackQuery();
    }
}
