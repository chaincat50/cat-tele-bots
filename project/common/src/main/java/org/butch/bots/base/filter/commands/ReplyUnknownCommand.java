package org.butch.bots.base.filter.commands;

import org.butch.bots.base.BotConsumer;
import org.butch.bots.base.helpers.send.DefaultSendMessageHelper;
import org.butch.bots.base.messages.CommandMessage;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class ReplyUnknownCommand implements BotConsumer<CommandMessage> {
    private final DefaultSendMessageHelper helper;

    public ReplyUnknownCommand(DefaultSendMessageHelper helper) {
        this.helper = helper;
    }

    @Override
    public void consume(CommandMessage commandMessage) throws TelegramApiException {
        helper.reply(commandMessage.getChatId(),
                commandMessage.getMessage().getMessageId(),
                "Неизвестная команда: " + commandMessage.getCommand());
    }
}
