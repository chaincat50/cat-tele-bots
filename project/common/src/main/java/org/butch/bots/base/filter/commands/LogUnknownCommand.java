package org.butch.bots.base.filter.commands;

import org.butch.bots.base.BotConsumer;
import org.butch.bots.base.messages.CommandMessage;
import org.slf4j.Logger;

public class LogUnknownCommand implements BotConsumer<CommandMessage> {
    private final Logger logger;

    public LogUnknownCommand(Logger logger) {
        this.logger = logger;
    }

    @Override
    public void consume(CommandMessage commandMessage) {
        logger.info("Unknown command: " + commandMessage);
    }
}
