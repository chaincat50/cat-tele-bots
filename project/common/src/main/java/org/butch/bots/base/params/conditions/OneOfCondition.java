package org.butch.bots.base.params.conditions;

import org.butch.bots.base.params.Result;
import org.butch.bots.base.params.terms.Term;

import java.util.Arrays;
import java.util.List;

public class OneOfCondition extends Condition {
    private final List<Condition> conditions;
    private Condition winner = null;
    private Result result = Result.PARTIAL_MATCH;

    public OneOfCondition(Condition... conditions) {
        this.conditions = Arrays.asList(conditions);
    }

    public static OneOfCondition create(Condition... conditions) {
        return new OneOfCondition(conditions);
    }

    @Override
    public Result accept(char c) throws IllegalStateException {
        if (result == Result.NO_MATCH)
            return Result.NO_MATCH;
        if (result == Result.EXACT_MATCH)
            throw new IllegalStateException("oneOf: ExactMatch");
        boolean anyMatch = false;
        for (Condition condition : conditions) {
            final Result r = condition.accept(c);
            if (r == Result.EXACT_MATCH) {
                result = Result.EXACT_MATCH;
                winner = condition;
                return Result.EXACT_MATCH;
            }
            if (r == Result.PARTIAL_MATCH) {
                anyMatch = true;
            }
        }
        if (!anyMatch) {
            result = Result.NO_MATCH;
            return Result.NO_MATCH;
        }
        return Result.PARTIAL_MATCH;
    }

    @Override
    public void reset() {
        for (Condition condition : conditions) {
            condition.reset();
        }
        winner = null;
        result = Result.PARTIAL_MATCH;
    }

    @Override
    public Term getTerm() throws IllegalStateException {
        if (result != Result.EXACT_MATCH)
            throw new IllegalStateException("oneOf");
        if (winner == null)
            throw new IllegalStateException("oneOf: winner = null");
        return winner.getTerm();
    }
}
