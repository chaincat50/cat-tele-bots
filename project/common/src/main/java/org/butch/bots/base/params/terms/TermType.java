package org.butch.bots.base.params.terms;

public enum TermType {
    STRING,
    INTEGER,
    LIST
}
