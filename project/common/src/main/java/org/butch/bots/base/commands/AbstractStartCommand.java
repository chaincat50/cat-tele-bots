package org.butch.bots.base.commands;

import org.butch.bots.base.Utils;
import org.butch.bots.base.messages.CommandMessage;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public abstract class AbstractStartCommand<TBot> extends Command<TBot> {
    public static final String COMMAND = "start";

    public AbstractStartCommand(TBot bot) {
        super(bot);
    }

    @Override
    public boolean addHelp() {
        return false;
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return "Начать работу";
    }

    @Override
    public String getHelp() {
        return "";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        final User user = msg.getMessage().getFrom();

        final String text;
        if (user == null) {
            text = getAnonymousGreeting();
        } else {
            text = getUserGreeting(user, Utils.getHtmlUserLink(user));
        }
        if (text != null && !text.isEmpty()) {
            sendHtml(msg.getChatId(), text);
        }
    }

    protected String getAnonymousGreeting() {
        return "Привет!";
    }

    protected String getUserGreeting(User user, String userLink) {
        return String.format("Привет, %s!", userLink);
    }

    protected abstract void sendHtml(String chatId, String text) throws TelegramApiException;
}
