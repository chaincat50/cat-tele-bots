package org.butch.bots.base.commands;

import org.butch.bots.base.Bot2;
import org.butch.bots.base.helpers.send.HtmlSendMessageHelper;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public abstract class BaseHelpCommand2<TBot extends Bot2> extends AbstractHelpCommand<TBot> {
    private final HtmlSendMessageHelper helper;

    public BaseHelpCommand2(TBot bot) {
        this(bot, new HtmlSendMessageHelper(bot));
    }

    public BaseHelpCommand2(TBot bot, HtmlSendMessageHelper helper) {
        super(bot);
        this.helper = helper;
    }

    @Override
    protected void sendHtml(String chatId, String text) throws TelegramApiException {
        helper.send(chatId, text);
    }
}
