package org.butch.bots.base;

import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@FunctionalInterface
public interface BotBiConsumer<TParam1, TParam2> {
    void consume(TParam1 param1, TParam2 param2) throws TelegramApiException;
}
