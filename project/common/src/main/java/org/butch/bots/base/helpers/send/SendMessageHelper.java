package org.butch.bots.base.helpers.send;

import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public interface SendMessageHelper {
    default Message send(String chatId,
                         String text) throws TelegramApiException {
        return send(chatId, text, null);
    }

    Message send(String chatId,
                 String text,
                 InlineKeyboardMarkup keyboard) throws TelegramApiException;

    Message reply(String chatId,
                  Integer messageId,
                  String text) throws TelegramApiException;

    default void edit(String chatId,
                      Integer messageId,
                      String text) throws TelegramApiException {
        edit(chatId, messageId, text, null);
    }

    void edit(String chatId,
              Integer messageId,
              String text,
              InlineKeyboardMarkup keyboard) throws TelegramApiException;
}
