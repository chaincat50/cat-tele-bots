package org.butch.bots.base.filter;

import org.butch.bots.base.Bot2;
import org.jetbrains.annotations.NotNull;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public interface Filter {
    @NotNull
    String getName();

    void accept(@NotNull Update update) throws TelegramApiException;

    default boolean register(@NotNull Bot2 bot) throws TelegramApiException {
        return true;
    }
}
