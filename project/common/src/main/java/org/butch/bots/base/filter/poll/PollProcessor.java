package org.butch.bots.base.filter.poll;

import org.butch.bots.base.BotConsumer;
import org.butch.bots.base.filter.MessageFilter;
import org.butch.bots.base.messages.PollMessage;
import org.jetbrains.annotations.NotNull;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class PollProcessor extends MessageFilter {
    private final BotConsumer<PollMessage> consumer;

    public PollProcessor(@NotNull BotConsumer<PollMessage> consumer) {
        this.consumer = consumer;
    }

    @Override
    @NotNull
    public String getName() {
        return "PollProcessor";
    }

    @Override
    protected void accept(@NotNull Update update,
                          @NotNull Message message,
                          @NotNull String chatId) throws TelegramApiException {
        if (!message.hasPoll())
            return;
        consumer.consume(new PollMessage(chatId, update));
    }
}
