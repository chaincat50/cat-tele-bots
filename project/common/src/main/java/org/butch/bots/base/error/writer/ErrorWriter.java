package org.butch.bots.base.error.writer;

import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public interface ErrorWriter {
    boolean accept(Update update, String filter, TelegramApiException ex);
}
