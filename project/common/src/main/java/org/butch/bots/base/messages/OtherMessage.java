package org.butch.bots.base.messages;

import org.telegram.telegrambots.meta.api.objects.Update;

public class OtherMessage extends BaseMessage {
    public OtherMessage(String chatId, Update update) {
        super(chatId, update);
    }

    @Override
    public Type getType() {
        return Type.OTHER_MESSAGE;
    }
}
