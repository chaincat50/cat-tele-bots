package org.butch.bots.base.commands;

import org.butch.bots.base.Bot;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class BaseStartCommand<TBot extends Bot> extends AbstractStartCommand<TBot> {
    public BaseStartCommand(TBot bot) {
        super(bot);
    }

    @Override
    protected void sendHtml(String chatId, String text) throws TelegramApiException {
        bot.sendHtml(chatId, text);
    }
}
