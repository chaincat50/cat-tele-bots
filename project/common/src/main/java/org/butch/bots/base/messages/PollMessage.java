package org.butch.bots.base.messages;

import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.polls.Poll;

public class PollMessage extends BaseMessage {
    public PollMessage(String chatId, Update update) {
        super(chatId, update);
    }

    @Override
    public Type getType() {
        return Type.POLL;
    }

    public Poll getPoll() {
        return getUpdate().getMessage().getPoll();
    }
}
