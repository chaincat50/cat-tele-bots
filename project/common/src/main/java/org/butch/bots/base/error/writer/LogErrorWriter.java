package org.butch.bots.base.error.writer;

import org.slf4j.Logger;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class LogErrorWriter implements ErrorWriter {
    private final Logger logger;

    public LogErrorWriter(Logger logger) {
        this.logger = logger;
    }

    @Override
    public boolean accept(Update update, String filter, TelegramApiException ex) {
        // TODO: consider adding appenders
        logger.warn(filter, ex);
        return true;
    }
}
