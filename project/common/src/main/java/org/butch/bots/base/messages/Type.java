package org.butch.bots.base.messages;

public enum Type {
    TEXT,
    COMMAND,
    POLL,
    CALLBACK_QUERY,
    INLINE,
    INLINE_SELECT,
    OTHER_MESSAGE,
    OTHER_UPDATE
}
