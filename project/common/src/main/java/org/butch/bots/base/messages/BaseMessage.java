package org.butch.bots.base.messages;

import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

public abstract class BaseMessage extends Base {
    private final String chatId;

    protected BaseMessage(String chatId, Update update) {
        super(update);
        this.chatId = chatId;
    }

    public String getChatId() {
        return chatId;
    }

    public String getUserId() {
        return Long.toString(getMessage().getFrom().getId());
    }

    public Message getMessage() {
        return getUpdate().getMessage();
    }
}
