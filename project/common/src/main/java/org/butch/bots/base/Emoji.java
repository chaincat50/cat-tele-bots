package org.butch.bots.base;

// https://www.unicode.org/emoji/charts/full-emoji-list.html
public class Emoji {
    public static class Uncategorized {
        public static String SMALL_BLACK_SQUARE = Character.toString(0x25AA);
        public static String SMALL_WHITE_SQUARE = Character.toString(0x25AB);
        public static String WHITE_SQUARE = Character.toString(0x25FB);
        public static String BLACK_SQUARE = Character.toString(0x25FC);
        public static String MEDIUM_WHITE_SQUARE = Character.toString(0x25FD);
        public static String MEDIUM_BLACK_SQUARE = Character.toString(0x25FE);
        public static String LARGE_BLACK_SQUARE = Character.toString(0x2B1B);
        public static String LARGE_WHITE_SQUARE = Character.toString(0x2B1C);
    }

    public static class PersonActivity {
        public static String PERSON_STANDING = Character.toString(0x1F9CD);
    }

    public static class Mail {
        public static String ENVELOPE = Character.toString(0x2709);
        public static String PACKAGE = Character.toString(0x1F4E6);
    }

    public static class Arrow {
        public static String UP = Character.toString(0x2B06);
        public static String RIGHT = Character.toString(0x27A1);
        public static String DOWN = Character.toString(0x2B07);
        public static String LEFT = Character.toString(0x2B05);
    }

    public static class OtherSymbol {
        public static String CHECK_MARK_BUTTON = Character.toString(0x2705);
        public static String CROSS_MARK = Character.toString(0x274C);
    }
}
