package org.butch.bots.base.helpers.send;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class HtmlSendMessageHelper implements SendMessageHelper {
    public static String PARSE_MODE = "HTML";

    private final TelegramLongPollingBot bot;

    public HtmlSendMessageHelper(TelegramLongPollingBot bot) {
        this.bot = bot;
    }

    public String escape(String str) {
        return str
                .replace("&", "&amp;")
                .replace("<", "&lt;")
                .replace(">", "&gt;");
    }

    @Override
    public Message send(String chatId,
                        String text,
                        InlineKeyboardMarkup keyboard) throws TelegramApiException {
        return bot.execute(SendMessage.builder()
                .chatId(chatId)
                .text(text)
                .parseMode(PARSE_MODE)
                .disableWebPagePreview(true)
                .replyMarkup(keyboard)
                .build());
    }

    @Override
    public Message reply(String chatId,
                         Integer messageId,
                         String text) throws TelegramApiException {
        return bot.execute(SendMessage.builder()
                .chatId(chatId)
                .replyToMessageId(messageId)
                .text(text)
                .parseMode(PARSE_MODE)
                .disableWebPagePreview(true)
                .build()
        );
    }

    @Override
    public void edit(String chatId,
                     Integer messageId,
                     String text,
                     InlineKeyboardMarkup keyboard) throws TelegramApiException {
        bot.execute(EditMessageText.builder()
                .chatId(chatId)
                .messageId(messageId)
                .text(text)
                .parseMode(PARSE_MODE)
                .disableWebPagePreview(true)
                .replyMarkup(keyboard)
                .build()
        );
    }
}
