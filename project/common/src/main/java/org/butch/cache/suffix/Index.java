package org.butch.cache.suffix;

import yuta.mori.sais;

public class Index {
    private final String str;
    private final int[] indices;

    public Index(String str) {
        this.str = str;
        this.indices = new int[str.length()];
        sais.suffixsort(str, indices, indices.length);
    }

    public boolean contains(String sub) {
        if (sub == null)
            return false;
        if (sub.isEmpty())
            return true;
        if (sub.length() > str.length())
            return false;

        int l = 0;
        int r = str.length() - 1;
        int lLen = getLcp(sub, l, 0);
        int rLen = getLcp(sub, r, 0);
        int mlr = Math.min(lLen, rLen);

        while (l < r && mlr < sub.length()) {
            mlr = Math.min(lLen, rLen);
            final int m = (r + l) / 2;
            final int mLen = getLcp(sub, m, mlr);
            if (mLen >= sub.length())
                return true;
            if (indices[m] + mLen >= str.length() ||
                    str.charAt(indices[m] + mLen) < sub.charAt(mLen)) {
                l = m + 1;
                lLen = getLcp(sub, l, mLen);
            } else {
                r = m;
                rLen = mLen;
            }
        }
        return Math.min(lLen, rLen) >= sub.length();
    }

    private int getLcp(String sub, int i, int currentLcp) {
        int ret = currentLcp;
        final int index = indices[i];
        while (ret < sub.length() &&
                index + ret < str.length() &&
                str.charAt(index + ret) == sub.charAt(ret)) {
            ++ret;
        }
        return ret;
    }
}
