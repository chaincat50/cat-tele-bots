package org.butch.cache.simple;

import org.butch.cache.SearchCache;

import java.util.*;

public class SimpleCache<TObj> implements SearchCache<TObj> {
    private final Map<String, TObj> map = new HashMap<>();

    @Override
    public void load(String text, TObj obj) {
        map.put(text.toLowerCase(Locale.ROOT), obj);
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public List<TObj> search(String mask) {
        final String maskLC = mask.toLowerCase(Locale.ROOT);
        final List<TObj> ret = new ArrayList<>();
        map.forEach((k, v) -> {
            if (k.contains(maskLC)) {
                ret.add(v);
            }
        });
        return ret;
    }
}
