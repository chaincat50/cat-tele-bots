package org.butch.bots.base.params;

import org.butch.bots.base.params.conditions.Condition;
import org.butch.bots.base.params.conditions.ConstCondition;
import org.butch.bots.base.params.conditions.IntegerCondition;
import org.butch.bots.base.params.conditions.OneOfCondition;
import org.butch.bots.base.params.terms.IntegerTerm;
import org.butch.bots.base.params.terms.StringTerm;
import org.butch.bots.base.params.terms.Term;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class ParamsParserTest {
    @Test(timeout = 2000)
    public void testIntegerParam() throws Exception {
        final Condition[] conditions = new Condition[] {
                IntegerCondition.create()
        };

        checkResult(ParamsParser.parse("123", conditions),
                123);

        checkResult(ParamsParser.parse("123q", conditions),
                "123q");

        checkResult(ParamsParser.parse("123 q", conditions),
                123, "q");

        checkResult(ParamsParser.parse("q 123", conditions),
                "q 123");

        checkResult(ParamsParser.parse("123 456", conditions),
                123, "456");

        checkResult(ParamsParser.parse("123 456",
                IntegerCondition.create(),
                IntegerCondition.create()
                ),
                123, 456);

        checkResult(ParamsParser.parse("123 456q",
                IntegerCondition.create(),
                IntegerCondition.create()
                ),
                "123 456q");
    }

    @Test(timeout = 2000)
    public void testConstParam() throws Exception {
        final String constValue = "123";
        final String constValue2 = "456";
        final Condition[] conditions = new Condition[] {
                ConstCondition.create(constValue)
        };

        checkResult(ParamsParser.parse("123", conditions),
                constValue);

        checkResult(ParamsParser.parse("123q", conditions),
                "123q");

        checkResult(ParamsParser.parse("123 q", conditions),
                constValue, "q");

        checkResult(ParamsParser.parse("q 123", conditions),
                "q 123");

        checkResult(ParamsParser.parse("123 456", conditions),
                constValue, "456");

        checkResult(ParamsParser.parse("123 456",
                ConstCondition.create(constValue),
                ConstCondition.create(constValue2)
                ),
                constValue, constValue2);

        checkResult(ParamsParser.parse("123 456q",
                ConstCondition.create(constValue),
                ConstCondition.create(constValue2)
                ),
                "123 456q");

        checkResult(ParamsParser.parse("12", conditions),
                "12");
    }

    @Test(timeout = 2000)
    public void testOneOfParam() throws Exception {
        final String constValue = "123";
        final String constValue2 = "456";
        final Condition[] conditions = new Condition[] {
                OneOfCondition.create(
                        ConstCondition.create(constValue),
                        IntegerCondition.create()
                )
        };

        checkResult(ParamsParser.parse("123", conditions),
                constValue);

        checkResult(ParamsParser.parse("890", conditions),
                890);

        checkResult(ParamsParser.parse("123q", conditions),
                "123q");

        checkResult(ParamsParser.parse("123 q", conditions),
                constValue, "q");

        checkResult(ParamsParser.parse("890 q", conditions),
                890, "q");

        checkResult(ParamsParser.parse("q 123", conditions),
                "q 123");
    }

    @Test(timeout = 2000)
    public void testLongSpaces() throws Exception {
        checkResult(ParamsParser.parse("  \t 123 456",
                IntegerCondition.create(),
                IntegerCondition.create()
                ),
                123, 456);

        checkResult(ParamsParser.parse("123  \t  456",
                IntegerCondition.create(),
                IntegerCondition.create()
                ),
                123, 456);

        checkResult(ParamsParser.parse("123  \t  456",
                ConstCondition.create("123"),
                ConstCondition.create("456")
                ),
                "123", "456");

        checkResult(ParamsParser.parse("123  \t  456",
                ConstCondition.create("123"),
                OneOfCondition.create(
                        ConstCondition.create("456"),
                        ConstCondition.create("789")
                )
                ),
                "123", "456");
    }

    @Test(timeout = 2000)
    public void testLineFeed() throws Exception {
        final ParserOptions options = new ParserOptions()
                .setFirstLine(true);

        checkResult(ParamsParser.parse("  \n 123 456",
                IntegerCondition.create(),
                IntegerCondition.create()
                ),
                123, 456);

        checkResult(ParamsParser.parse("123  \n  456",
                IntegerCondition.create(),
                IntegerCondition.create()
                ),
                123, 456);

        checkResult(ParamsParser.parse("123  \n  456",
                ConstCondition.create("123"),
                ConstCondition.create("456")
                ),
                "123", "456");

        checkResult(ParamsParser.parse("123  \n  456",
                ConstCondition.create("123"),
                OneOfCondition.create(
                        ConstCondition.create("456"),
                        ConstCondition.create("789")
                )
                ),
                "123", "456");

        checkResult(ParamsParser.parse(" 1\nqwe", options,
                OneOfCondition.create(
                        IntegerCondition.create(),
                        ConstCondition.create("all")
                )
                ),
                1, "qwe"
        );
    }

    @Test(timeout = 2000)
    public void testFirstLine() throws Exception {
        final ParserOptions options = new ParserOptions()
                .setFirstLine(true);

        checkResult(ParamsParser.parse("123  \n  456", options,
                IntegerCondition.create()
                ),
                123, "456");

        checkResult(ParamsParser.parse("123  \n  456", options,
                IntegerCondition.create(),
                IntegerCondition.create()
                ),
                "123  \n  456");

        checkResult(ParamsParser.parse("  \n  456", options,
                IntegerCondition.create()
                ),
                "  \n  456");

        checkResult(ParamsParser.parse("q  \n  456", options,
                IntegerCondition.create()
                ),
                "q  \n  456");

        checkResult(ParamsParser.parse("123  \n  456", options,
                OneOfCondition.create(
                        IntegerCondition.create(),
                        ConstCondition.create("all")
                )
                ),
                123, "456");

        checkResult(ParamsParser.parse("all  \n  456", options,
                OneOfCondition.create(
                        IntegerCondition.create(),
                        ConstCondition.create("all")
                )
                ),
                "all", "456");

        checkResult(ParamsParser.parse("  \n  456", options,
                OneOfCondition.create(
                        IntegerCondition.create(),
                        ConstCondition.create("all")
                )
                ),
                "  \n  456");

        checkResult(ParamsParser.parse("123 q  \n  456", options,
                        IntegerCondition.create()
                ),
                123, "q  \n  456");

    }

    private void checkResult(List<Term> result, Object... expected) {
        Assert.assertEquals(expected.length, result.size());
        for (int i = 0; i < expected.length; ++i) {
            final Object ex = expected[i];
            if (ex.getClass() == String.class) {
                checkStringTerm((String) ex, result.get(i));
            } else if (ex instanceof Number) {
                checkIntegerTerm(((Number) ex).longValue(), result.get(i));
            }
        }
    }

    private void checkIntegerTerm(long value, Term term) {
        Assert.assertEquals(IntegerTerm.class, term.getClass());
        final IntegerTerm integerTerm = (IntegerTerm) term;
        Assert.assertEquals(value, integerTerm.getValue());
    }

    private void checkStringTerm(String value, Term term) {
        Assert.assertEquals(StringTerm.class, term.getClass());
        final StringTerm stringTerm = (StringTerm) term;
        Assert.assertEquals(value, stringTerm.getValue());
    }
}
