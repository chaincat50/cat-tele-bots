package org.butch.bots.poll.commands;

import org.butch.bots.base.commands.Command;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.poll.PollBot;
import org.telegram.telegrambots.meta.api.methods.polls.SendPoll;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

public class CreateCommand extends Command<PollBot> {
    private static final String POLL_PARAM_DURATION = "duration=";
    private static final String POLL_ANSWER_PREFIX = "-";

    public CreateCommand(PollBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "create";
    }

    @Override
    public String getDescription() {
        return "Создать опрос";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " [необязательные параметры]\n" +
                "Вопрос\n" +
                "- Ответ1\n" +
                "- Ответ2\n" +
                "...\n" +
                "\n" +
                "Создаёт опрос.\n" +
                "\n" +
                "Параметры:\n" +
                "<b>" + POLL_PARAM_DURATION + "</b><i>&lt;число секунд&gt;</i>" +
                " - задаёт время до автоматического закрытия опроса, от 5с до 600с.\n";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        final String[] lines = msg.getParams()
                .lines()
                .toArray(String[]::new);

        Integer duration = null;
        if (lines.length > 0) {
            final String[] params = lines[0].split(" ");
            for (String param : params) {
                final String trimmed = param.trim();
                if (trimmed.startsWith(POLL_PARAM_DURATION)) {
                    final String v = trimmed.substring(POLL_PARAM_DURATION.length());
                    try {
                        duration = Integer.parseInt(v);
                    } catch (NumberFormatException e) {
                        throw new TelegramApiException("Неправильный параметр '" + v + "' - ожидается количество секунд.");
                    }
                }
            }
        }

        final StringBuilder builder = new StringBuilder();
        String question = null;
        final List<String> options = new ArrayList<>();
        boolean isQuestion = true;
        for (int i = 1; i < lines.length; ++i) {
            final String line = lines[i];
            if (line.startsWith(POLL_ANSWER_PREFIX)) {
                if (builder.length() > 0) {
                    if (isQuestion) {
                        question = builder.toString();
                    } else {
                        options.add(builder.toString());
                    }
                    builder.setLength(0);
                }
                final String trimmed = line.substring(POLL_ANSWER_PREFIX.length()).trim();
                if (trimmed.length() > 0) {
                    builder.append(trimmed).append("\n");
                }
                isQuestion = false;
            } else {
                final String trimmed = line.trim();
                if (trimmed.length() > 0) {
                    builder.append(trimmed).append("\n");
                }
            }
        }
        if (builder.length() > 0) {
            if (isQuestion) {
                question = builder.toString();
            } else {
                options.add(builder.toString());
            }
        }
        if (question == null || question.isEmpty()) {
            throw new TelegramApiException("Ошибка в запросе. Вопрос не может быть пустым.");
        } else if (options.size() < 2 || options.size() > 10) {
            throw new TelegramApiException("Ошибка в запросе. Должно быть от 2 до 10 ответов.");
        } else {
            final SendPoll request = new SendPoll();
            request.setChatId(msg.getChatId());
            request.setQuestion(question);
            request.setOptions(options);
            request.setIsAnonymous(true);
            request.setType("regular");
            request.setAllowMultipleAnswers(true);
            if (duration != null) {
                request.setOpenPeriod(duration);
            }
            bot.execute(request);
        }
    }
}
