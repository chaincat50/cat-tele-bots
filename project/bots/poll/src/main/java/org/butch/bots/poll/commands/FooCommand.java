package org.butch.bots.poll.commands;

import org.butch.bots.base.commands.Command;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.poll.PollBot;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class FooCommand extends Command<PollBot> {
    public FooCommand(PollBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "foo";
    }

    @Override
    public String getDescription() {
        return "";
    }

    @Override
    public String getHelp() {
        return "";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
    }
}
