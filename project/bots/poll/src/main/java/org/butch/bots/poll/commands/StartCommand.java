package org.butch.bots.poll.commands;

import org.butch.bots.base.commands.BaseStartCommand;
import org.butch.bots.poll.PollBot;

public class StartCommand extends BaseStartCommand<PollBot> {
    public StartCommand(PollBot bot) {
        super(bot);
    }
}
