package org.butch.bots.poll.commands;

import org.butch.bots.base.commands.BaseHelpCommand;
import org.butch.bots.poll.PollBot;

public class HelpCommand extends BaseHelpCommand<PollBot> {
    public HelpCommand(PollBot bot) {
        super(bot);
    }

    @Override
    public String getHelp() {
        return "" +
                "<u><b>Описание работы бота.</b></u>\n" +
                "\n" +
                "Если перекинуть в бота опрос, он напечатает результат в формате, удобном для копирования. " +
                "Если вместо результатов бот печатает нули - опрос надо закрыть.\n" +
                "\n" +
                "<u>Список поддерживаемых команд:</u>\n";
    }
}
