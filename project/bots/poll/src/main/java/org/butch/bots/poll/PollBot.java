package org.butch.bots.poll;

import org.butch.bots.base.Bot;
import org.butch.bots.base.messages.PollMessage;
import org.butch.bots.poll.commands.CreateCommand;
import org.butch.bots.poll.commands.HelpCommand;
import org.butch.bots.poll.commands.StartCommand;
import org.telegram.telegrambots.meta.api.objects.polls.Poll;
import org.telegram.telegrambots.meta.api.objects.polls.PollOption;

public class PollBot extends Bot {
    private final String token;

    public PollBot(String token) {
        this.token = token;
        addCommand(new HelpCommand(this));
        addCommand(new StartCommand(this));
        addCommand(new CreateCommand(this));
    }

    @Override
    public String getBotUsername() {
        return "PollBot";
    }

    @Override
    public String getBotToken() {
        return token;
    }

    @Override
    protected void processPoll(PollMessage msg) {
        final Poll poll = msg.getPoll();
        final StringBuilder res1 = new StringBuilder();
        res1.append(poll.getQuestion()).append("\n\n");
        final StringBuilder res2 = new StringBuilder();
        for (PollOption option : poll.getOptions()) {
            res1.append(option.getText())
                    .append(": ")
                    .append(option.getVoterCount())
                    .append("\n");
            res2.append(option.getVoterCount()).append("\t");
        }
        res1.append("\n").append("Всего: ").append(poll.getTotalVoterCount())
                .append("\n\n").append(res2);

        sendMessageOrError(msg.getChatId(), res1.toString());
    }
}
