package org.butch.bots.reminder.repository;

public class Chat {
    private String chatId;
    private String timeZone;

    public Chat() {
    }

    public Chat(String chatId) {
        this.chatId = chatId;
        this.timeZone = "UTC";
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    @Override
    public String toString() {
        return "" +
                " chatId = " + chatId +
                " timeZone = " + timeZone
                ;
    }
}
