package org.butch.bots.reminder.repository;

public enum SubType {
    AT(1),
    DURATION(2),
    ONCE(3);

    private final int number;

    SubType(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public static SubType fromNumber(int number) {
        switch (number) {
            case 1:
                return AT;

            case 2:
                return DURATION;

            case 3:
                return ONCE;
        }

        throw new IllegalArgumentException("Unsupported value: " + number);
    }
}
