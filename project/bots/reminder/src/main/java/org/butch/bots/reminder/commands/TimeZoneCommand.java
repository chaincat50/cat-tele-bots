package org.butch.bots.reminder.commands;

import org.butch.bots.base.commands.Command;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.reminder.ReminderBot;
import org.butch.bots.reminder.repository.Chat;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.TimeZone;

public class TimeZoneCommand extends Command<ReminderBot> {
    private final static String DEFAULT_TZ = "GMT";

    public TimeZoneCommand(ReminderBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "tz";
    }

    @Override
    public String getDescription() {
        return "[часовой пояс] Получить/задать часовой пояс";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " [часовой пояс]\n" +
                "\n" +
                "Задаёт часовой пояс, либо возвращает текущий.\n" +
                "\n" +
                "Параметры:\n" +
                "<b>часовой пояс</b> - часовой пояс (например: <b>GMT+3</b>). " +
                "Если параметр задан - часовой пояс будет установлен. " +
                "Если параметр отсутствует - текущий часовой пояс будет напечатан.\n";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        final String params = msg.getParams().trim();
        final Chat chat = bot.getChat(msg.getChatId());
        if (params.isEmpty()) {
            bot.sendMessage(msg.getChatId(), "Текущий часовой пояс: " + chat.getTimeZone());
        } else {
            if (!bot.checkForAdmin(
                    msg.getMessage().getChat(),
                    msg.getMessage().getFrom()))
                return;

            if (isValidTimeZone(params)) {
                chat.setTimeZone(params);
                if (bot.updateChat(chat)) {
                    bot.sendMessage(msg.getChatId(), "Часовой пояс задан: " + chat.getTimeZone());
                } else {
                    bot.sendMessage(msg.getChatId(), "При установке часового пояса возникла ошибка");
                }
            } else {
                bot.sendMessage(msg.getChatId(), "Неизвестный часовой пояс: " + chat.getTimeZone());
            }
        }
    }

    private static boolean isValidTimeZone(String tz) {
        return DEFAULT_TZ.equals(tz) || !DEFAULT_TZ.equals(TimeZone.getTimeZone(tz).getID());
    }
}
