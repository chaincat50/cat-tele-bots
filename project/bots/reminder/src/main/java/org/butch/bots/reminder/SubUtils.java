package org.butch.bots.reminder;

import org.butch.bots.reminder.repository.Chat;
import org.butch.bots.reminder.repository.Sub;
import org.butch.utils.TimeUtils;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class SubUtils {
    private SubUtils() {}

    public static String humanReadable(Sub sub, Chat chat) {
        switch (sub.getType()) {
            case AT:
                return String.format("Каждый день в %s: %s", timeAt(sub, chat), sub.getMessage());

            case DURATION:
                return String.format("Каждые %s: %s", timeDuration(sub), sub.getMessage());

            case ONCE:
                return String.format("Один раз в %s: %s", onceAt(sub), sub.getMessage());
        }

        return "Напоминание неизвестного типа. Сообщение: " + sub.getMessage();
    }

    private static String timeAt(Sub sub, Chat chat) {
        final Calendar calendar = new GregorianCalendar();
        calendar.setTimeZone(TimeZone.getTimeZone(chat.getTimeZone()));
        calendar.setTimeInMillis(sub.getTime());
        return String.format("%tH:%tM", calendar, calendar);
    }

    private static String timeDuration(Sub sub) {
        return String.format("%dс", sub.getTime() / TimeUtils.MS_IN_SECOND);
    }

    // TODO: Change
    private static String onceAt(Sub sub) {
        final long hours = sub.getTime() / TimeUtils.MS_IN_HOUR;
        final long minutes = (sub.getTime() % TimeUtils.MS_IN_HOUR) / TimeUtils.MS_IN_MINUTE;
        final long seconds = (sub.getTime() % TimeUtils.MS_IN_MINUTE) / TimeUtils.MS_IN_SECOND;
        return String.format("%d:%d:%d", hours, minutes, seconds);
    }
}
