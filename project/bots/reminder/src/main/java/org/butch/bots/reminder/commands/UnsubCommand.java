package org.butch.bots.reminder.commands;

import org.butch.bots.base.commands.Command;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.reminder.ReminderBot;
import org.butch.bots.reminder.SubUtils;
import org.butch.bots.reminder.repository.Chat;
import org.butch.bots.reminder.repository.Sub;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class UnsubCommand extends Command<ReminderBot> {
    public UnsubCommand(ReminderBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "unsub";
    }

    @Override
    public String getDescription() {
        return "<номер> Удалить подписку";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " &lt;номер&gt;\n" +
                "\n" +
                "Удаляет подписку на напоминание любого типа.\n" +
                "\n" +
                "Параметры:\n" +
                "<b>номер</b> - порядковый номер напоминания в списке.\n";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        if (!bot.checkForAdmin(
                msg.getMessage().getChat(),
                msg.getMessage().getFrom()))
            return;

        final String params = msg.getParams().trim();
        if (params.isEmpty())
            throw new TelegramApiException("Не хватает параметров");

        final int num;
        try {
            num = Integer.parseUnsignedInt(params);
            if (num == 0)
                throw new NumberFormatException();
        } catch (NumberFormatException ignored) {
            throw new TelegramApiException("Неправильный номер: " + params);
        }

        final Sub sub = bot.removeSub(msg.getChatId(), num);
        if (sub != null) {
            final Chat chat = bot.getChat(msg.getChatId());
            bot.sendMessage(msg.getChatId(), String.format("Подписка удалена: %s", SubUtils.humanReadable(sub, chat)));
        } else {
            bot.sendMessage(msg.getChatId(), String.format("Подписка %d не найдена.", num));
        }
    }
}
