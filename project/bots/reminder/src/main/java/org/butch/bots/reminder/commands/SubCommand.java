package org.butch.bots.reminder.commands;

import org.butch.bots.base.commands.Command;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.reminder.ReminderBot;
import org.butch.bots.reminder.SubUtils;
import org.butch.bots.reminder.repository.Chat;
import org.butch.bots.reminder.repository.Sub;
import org.butch.utils.TimeUtils;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class SubCommand extends Command<ReminderBot> {
    public SubCommand(ReminderBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "sub";
    }

    @Override
    public String getDescription() {
        return "<период> <сообщение> Подписаться на периодические напоминания";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " &lt;промежуток&gt; &lt;сообщение&gt;\n" +
                "\n" +
                "Создаёт регулярные напоминания через заданный промежуток времени.\n" +
                "\n" +
                "Праметры:\n" +
                "<b>промежуток</b> - промежуток времени между напоминаниями, не менее 5с.\n" +
                "<b>сообщение</b> - текстовое сообщение, которое бот будет писать через " +
                "заданный промежуток времени.\n";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        if (!bot.checkForAdmin(
                msg.getMessage().getChat(),
                msg.getMessage().getFrom()))
            return;

        final String params = msg.getParams().trim();
        final int index = params.indexOf(" ");
        if (index < 0)
            throw new TelegramApiException("Не хватает параметров");

        final String timeStr = params.substring(0, index);
        final String message = params.substring(index + 1).trim();

        final long time;
        try {
            time = Long.parseUnsignedLong(timeStr);
        } catch (NumberFormatException ignored) {
            throw new TelegramApiException("Неправильный формат времени: '" + timeStr + "'");
        }
        if (time < 5)
            throw new TelegramApiException("Время не может быть меньше 5с");

        final Sub sub = Sub.sub(msg.getChatId(), message, time * TimeUtils.MS_IN_SECOND);
        final int num = bot.addSub(sub);
        if (num < 0) {
            if (num == ReminderBot.DATABASE_ERROR)
                throw new TelegramApiException("Ошибка записи в базу данных");
            if (num == ReminderBot.TOO_MANY_SUBS_ERROR)
                throw new TelegramApiException("Слишком много подписок");
            throw new TelegramApiException("При добавлении напоминания возникла ошибка");
        } else {
            final Chat chat = bot.getChat(msg.getChatId());
            bot.sendMessage(msg.getChatId(),
                    String.format("" +
                            "Добавлена подписка:\n" +
                            "%d. %s", num, SubUtils.humanReadable(sub, chat)
                    )
            );
        }
    }
}
