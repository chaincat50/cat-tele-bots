package org.butch.bots.reminder;

import org.butch.bots.base.Bot;
import org.butch.bots.reminder.commands.*;
import org.butch.bots.reminder.repository.Chat;
import org.butch.bots.reminder.repository.Storage;
import org.butch.bots.reminder.repository.Sub;
import org.butch.utils.TimeUtils;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ReminderBot extends Bot {
    public static final int MAX_REMINDERS_PER_CHAT = 10;
    public static final int DATABASE_ERROR = -1;
    public static final int TOO_MANY_SUBS_ERROR = -2;

    private final String token;

    private final Storage storage;
    private final HashMap<String, Chat> chats = new HashMap<>();
    private final HashMap<String, List<Sub>> subsByChat = new HashMap<>();

    public ReminderBot(String token, Storage storage) {
        this.token = token;
        this.storage = storage;
        this.storage.init();

        addCommand(new HelpCommand(this));
        addCommand(new StartCommand(this));
        addCommand(new TimeZoneCommand(this));
        addCommand(new ListCommand(this));
        addCommand(new SubCommand(this));
        addCommand(new SubAtCommand(this));
        addCommand(new UnsubCommand(this));
    }

    @Override
    public String getBotUsername() {
        return "ReminderBot";
    }

    @Override
    public String getBotToken() {
        return token;
    }

    @Override
    public void onTimer(long time) {
        synchronized (storage) {
            for (List<Sub> list : subsByChat.values()) {
                for (Sub sub : list) {
                    try {
                        if (tryProcessTime(sub, time)) {
                            if (!storage.updateSub(sub)) {
                                LOGGER.warn("Error updating sub: " + sub);
                            }
                        }
                    } catch (Throwable ex) {
                        LOGGER.error("Timer: " + sub.toString(), ex);
                    }
                }
            }
        }
    }

    @Override
    public void onRegister() {
        super.onRegister();
        synchronized (storage) {
            final List<Chat> chats = storage.queryChats();
            for (Chat chat : chats) {
                this.chats.put(chat.getChatId(), chat);
            }
            final List<Sub> subs = storage.querySubs();
            for (Sub sub : subs) {
                final List<Sub> list = subsByChat.computeIfAbsent(sub.getChatId(), k -> new ArrayList<>());
                list.add(sub);
            }
        }
    }

    @Override
    protected void processLeftGroupUser(String chatId, User user) {
        if (user.getId() != null && user.getId().equals(getBotUser().getId())) {
            LOGGER.info("leftGroup: " + chatId);
            if (!removeChat(chatId)) {
                LOGGER.warn("Could not remove chat: " + chatId);
            }
            if (!removeSubs(chatId)) {
                LOGGER.warn("Could not remove subs: " + chatId);
            }
        }
    }

    public Chat getChat(String chatId) {
        LOGGER.info("getChat: " + chatId);
        synchronized (storage) {
            final Chat ret = chats.get(chatId);
            if (ret != null)
                return ret;
        }
        return new Chat(chatId);
    }

    public boolean updateChat(Chat chat) {
        LOGGER.info("updateChat: " + chat);
        synchronized (storage) {
            final Chat stored = chats.get(chat.getChatId());
            if (stored == null) {
                if (!storage.insertChat(chat))
                    return false;
            } else {
                if (!storage.updateChat(chat))
                    return false;
            }
            chats.put(chat.getChatId(), chat);
            return true;
        }
    }

    public boolean removeChat(String chatId) {
        LOGGER.info("removeChat: " + chatId);
        synchronized (storage) {
            final Chat stored = chats.remove(chatId);
            if (stored == null)
                return true;
            return storage.deleteChat(stored);
        }
    }

    public int addSub(Sub sub) {
        LOGGER.info("addSub: " + sub);
        synchronized (storage) {
            final int id = storage.insertSub(sub);
            if (id < 0)
                return DATABASE_ERROR;
            sub.setId(id);
            final List<Sub> list = subsByChat.computeIfAbsent(sub.getChatId(), k -> new ArrayList<>());
            if (list.size() >= MAX_REMINDERS_PER_CHAT)
                return TOO_MANY_SUBS_ERROR;
            list.add(sub);
            return list.size();
        }
    }

    public boolean removeSubs(String chatId) {
        LOGGER.info("removeSubs: " + chatId);
        synchronized (storage) {
            final List<Sub> list = subsByChat.remove(chatId);
            if (list == null)
                return true;
            boolean ret = true;
            for (Sub sub : list) {
                if (!storage.deleteSub(sub)) {
                    ret = false;
                }
            }
            return ret;
        }
    }

    public Sub removeSub(String chatId, int num) {
        LOGGER.info("removeSub: " + chatId + ", " + num);
        synchronized (storage) {
            final List<Sub> list = subsByChat.get(chatId);
            if (list == null)
                return null;
            if (num <= 0 || list.size() < num)
                return null;
            final Sub sub = list.remove(num - 1);
            storage.deleteSub(sub);
            return sub;
        }
    }

    public List<Sub> getSubs(String chatId) {
        LOGGER.info("getSubs: " + chatId);
        synchronized (storage) {
            return subsByChat.get(chatId);
        }
    }

    // TODO: Replace with inheritance
    private boolean tryProcessTime(Sub sub, long time) throws TelegramApiException {
        switch (sub.getType()) {
            case AT:
                return tryProcessAt(sub, time);

            case DURATION:
                return tryProcessDuration(sub, time);

            case ONCE:
                return tryProcessOnce(sub, time);
        }
        return false;
    }

    private boolean tryProcessAt(Sub sub, long time) throws TelegramApiException {
        final long expected = TimeUtils.getDate(sub.getLastExecuteTime()) +
                TimeUtils.MS_IN_DAY + sub.getTime();
        if (time < expected)
            return false;

        LOGGER.info("Expected: " + expected);
        LOGGER.info("Time: " + time);

        sub.setLastExecuteTime(time);
        sendMessage(sub.getChatId(), sub.getMessage());
        return true;
    }

    private boolean tryProcessDuration(Sub sub, long time) throws TelegramApiException {
        if (time - sub.getLastExecuteTime() < sub.getTime())
            return false;

        sub.setLastExecuteTime(time);
        sendMessage(sub.getChatId(), sub.getMessage());
        return true;
    }

    private boolean tryProcessOnce(Sub sub, long time) {
        return false;
    }
}
