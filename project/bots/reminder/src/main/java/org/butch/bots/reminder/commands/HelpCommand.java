package org.butch.bots.reminder.commands;

import org.butch.bots.base.commands.BaseHelpCommand;
import org.butch.bots.reminder.ReminderBot;

public class HelpCommand extends BaseHelpCommand<ReminderBot> {
    public HelpCommand(ReminderBot bot) {
        super(bot);
    }

    @Override
    public String getHelp() {
        return "" +
                "<u><b>Описание работы бота.</b></u>\n" +
                "\n" +
                "Бот предназначен для управления напоминаниями.\n" +
                "Детали о напоминаниях, включая текст сообщения, хранятся в базе данных.\n" +
                "<b>Ограничение:</b> запрещено иметь больше чем " +
                ReminderBot.MAX_REMINDERS_PER_CHAT +
                " подписок на напоминания одновременно.\n" +
                "\n" +
                "<u>Список поддерживаемых команд:</u>\n";
    }
}
