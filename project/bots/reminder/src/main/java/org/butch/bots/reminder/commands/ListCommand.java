package org.butch.bots.reminder.commands;

import org.butch.bots.base.commands.Command;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.reminder.ReminderBot;
import org.butch.bots.reminder.SubUtils;
import org.butch.bots.reminder.repository.Chat;
import org.butch.bots.reminder.repository.Sub;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.List;

public class ListCommand extends Command<ReminderBot> {
    public ListCommand(ReminderBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "list";
    }

    @Override
    public String getDescription() {
        return "Список подписок";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + "\n" +
                "\n" +
                "Печатает список существующих подписок.\n";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        final Chat chat = bot.getChat(msg.getChatId());
        final List<Sub> subs = bot.getSubs(msg.getChatId());
        if (subs == null || subs.isEmpty()) {
            bot.sendMessage(msg.getChatId(), "Список напоминаний пуст.");
        } else {
            final StringBuilder builder = new StringBuilder();
            builder.append("Список напоминаний: \n\n");
            for (int i = 0; i < subs.size(); ++i) {
                final Sub sub = subs.get(i);
                builder.append(String.format("%d. %s\n", i + 1, SubUtils.humanReadable(sub, chat)));
            }
            bot.sendHtml(msg.getChatId(), builder.toString());
        }
    }
}
