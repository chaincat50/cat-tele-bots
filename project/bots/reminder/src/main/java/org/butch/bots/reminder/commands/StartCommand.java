package org.butch.bots.reminder.commands;

import org.butch.bots.base.commands.BaseStartCommand;
import org.butch.bots.reminder.ReminderBot;

public class StartCommand extends BaseStartCommand<ReminderBot> {
    public StartCommand(ReminderBot bot) {
        super(bot);
    }
}
