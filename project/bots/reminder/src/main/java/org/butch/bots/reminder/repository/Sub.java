package org.butch.bots.reminder.repository;

import org.butch.utils.TimeUtils;

import java.util.TimeZone;

public class Sub {
    private int id;
    private String chatId;
    private SubType type;
    private String message;
    private long time;
    private long lastExecuteTime;

    public static Sub sub(String chatId, String message, long time) {
        final Sub sub = new Sub();
        sub.setChatId(chatId);
        sub.setMessage(message);
        sub.setTime(time);
        sub.setType(SubType.DURATION);
        return sub;
    }

    public static Sub subAt(String chatId, String message, long time, String timeZone) {
        final long now = System.currentTimeMillis();

        final Sub sub = new Sub();
        sub.setChatId(chatId);
        sub.setMessage(message);
        time -= TimeZone.getTimeZone(timeZone).getOffset(now);
        while (time < 0) {
            time += TimeUtils.MS_IN_DAY;
        }
        sub.setTime(time);
        sub.setType(SubType.AT);

        // Setting LastExecuteTime so that the first execute occurred on time.
        final long today = TimeUtils.getDate(now);
        if (time <= now - today) {
            sub.setLastExecuteTime(today + time);
        } else {
            sub.setLastExecuteTime(today + time - TimeUtils.MS_IN_DAY);
        }
        return sub;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public SubType getType() {
        return type;
    }

    public void setType(SubType type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public long getLastExecuteTime() {
        return lastExecuteTime;
    }

    public void setLastExecuteTime(long lastExecuteTime) {
        this.lastExecuteTime = lastExecuteTime;
    }

    @Override
    public String toString() {
        return "" +
                " id = " + id +
                " chatId = " + chatId +
                " type = " + type +
                " message = " + message +
                " time = " + time +
                " lastExecuteTime = " + lastExecuteTime
                ;
    }
}
