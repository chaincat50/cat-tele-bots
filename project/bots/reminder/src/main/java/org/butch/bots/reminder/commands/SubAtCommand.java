package org.butch.bots.reminder.commands;

import org.butch.bots.base.commands.Command;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.reminder.ReminderBot;
import org.butch.bots.reminder.SubUtils;
import org.butch.bots.reminder.repository.Chat;
import org.butch.bots.reminder.repository.Sub;
import org.butch.utils.TimeUtils;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SubAtCommand extends Command<ReminderBot> {
    private static final Pattern HH_MM = Pattern.compile("(\\d?\\d):(\\d\\d)");

    public SubAtCommand(ReminderBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "at";
    }

    @Override
    public String getDescription() {
        return "<ЧЧ:ММ> <сообщение> Подписаться на ежедневные напоминания";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " &lt;время&gt; &lt;сообщение&gt;\n" +
                "\n" +
                "Создаёт ежедневные напоминания.\n" +
                "\n" +
                "Параметры:\n" +
                "<b>время</b> - время напоминания в формате ЧЧ:ММ.\n" +
                "<b>сообщение</b> - текстовое сообщение, которое бот будет писать " +
                "каждый день в заданное время.\n";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        if (!bot.checkForAdmin(
                msg.getMessage().getChat(),
                msg.getMessage().getFrom()))
            return;

        final String params = msg.getParams().trim();
        final int index = params.indexOf(" ");
        if (index < 0)
            throw new TelegramApiException("Не хватает параметров");

        final String timeStr = params.substring(0, index);
        final String message = params.substring(index + 1).trim();

        final Matcher matcher = HH_MM.matcher(timeStr);
        if (!matcher.matches())
            throw new TelegramApiException("Неправильный формат времени: '" + timeStr + "'");
        final String hoursStr = matcher.group(1);
        final String minutesStr = matcher.group(2);

        final long time;
        try {
            final long hours = Long.parseUnsignedLong(hoursStr);
            final long minutes = Long.parseUnsignedLong(minutesStr);
            time = hours * TimeUtils.MS_IN_HOUR + minutes * TimeUtils.MS_IN_MINUTE;
        } catch (NumberFormatException ignored) {
            throw new TelegramApiException("Неправильный формат времени: '" + timeStr + "'");
        }
        if (time > TimeUtils.MAX_TIME)
            throw new TelegramApiException("Время не может быть меньше 5с");

        final Chat chat = bot.getChat(msg.getChatId());
        final Sub sub = Sub.subAt(msg.getChatId(), message, time, chat.getTimeZone());
        final int num = bot.addSub(sub);
        if (num < 0) {
            if (num == ReminderBot.DATABASE_ERROR)
                throw new TelegramApiException("Ошибка записи в базу данных");
            if (num == ReminderBot.TOO_MANY_SUBS_ERROR)
                throw new TelegramApiException("Слишком много подписок");
            throw new TelegramApiException("При добавлении напоминания возникла ошибка");
        } else {
            bot.sendMessage(msg.getChatId(),
                    String.format("" +
                            "Добавлена подписка:\n" +
                            "%d. %s", num, SubUtils.humanReadable(sub, chat)
                    )
            );
        }
    }
}
