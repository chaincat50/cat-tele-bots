package org.butch.bots.reminder.repository;

import java.util.List;

public interface Storage {
    void init();

    boolean insertChat(Chat chat);
    boolean updateChat(Chat chat);
    boolean deleteChat(Chat chat);
    List<Chat> queryChats();

    int insertSub(Sub sub);
    boolean updateSub(Sub sub);
    boolean deleteSub(Sub sub);
    List<Sub> querySubs();
}
