package org.butch.bots.feedback;

import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@FunctionalInterface
public interface TelegramConsumer<T> {
    void consume(T param) throws TelegramApiException;
}
