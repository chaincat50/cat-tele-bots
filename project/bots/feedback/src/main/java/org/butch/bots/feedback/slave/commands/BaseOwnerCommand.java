package org.butch.bots.feedback.slave.commands;

import org.butch.bots.feedback.slave.BaseSlaveBot;

public abstract class BaseOwnerCommand extends OwnerCommand<BaseSlaveBot> {
    public BaseOwnerCommand(BaseSlaveBot bot) {
        super(bot);
    }
}
