package org.butch.bots.feedback.slave.basic;

import org.butch.bots.base.messages.BaseMessage;
import org.butch.bots.base.messages.OtherMessage;
import org.butch.bots.base.messages.PollMessage;
import org.butch.bots.base.messages.TextMessage;
import org.butch.bots.feedback.Result;
import org.butch.bots.feedback.TelegramConsumer;
import org.butch.bots.feedback.master.FeedbackMasterBot;
import org.butch.bots.feedback.master.repository.BotDto;
import org.butch.bots.feedback.slave.BaseSlaveBot;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Objects;
import java.util.function.Consumer;

public class FeedbackSlaveBot extends BaseSlaveBot {
    public FeedbackSlaveBot(FeedbackMasterBot masterBot, BotDto data) {
        super(masterBot, data);

        addCommand(new HelpCommand(this));
        addCommand(new SecureCommand(this));
    }

    @Override
    public String getBotUsername() {
        return "FeedbackSlaveBot #" + getData().getId();
    }

    @Override
    public void onUpdateReceived(Update update) {
        super.onUpdateReceived(update);

        if (!update.hasMessage())
            return;

        final Message message = update.getMessage();
        final String userId = Long.toString(update.getMessage().getFrom().getId());
        final String chatId = Long.toString(update.getMessage().getChatId());

        if (isBanned(userId) || isBanned(chatId))
            return; // Ignore banned users and chats completely

        if (Objects.equals(userId, chatId) &&
                !Objects.equals(getData().getChatId(), chatId)) {
            // Personal message - forward to the chat
            try {
                final String messageId = forwardMessage(getData().getChatId(), message);
                if (!getMasterBot().insertChatId(getData().getChatId(), messageId, chatId)) {
                    LOGGER.error("Error inserting chatId.");
                }
            } catch (TelegramApiException ex) {
                onError(chatId, ex);
            }
        }
    }

    @Override
    protected void processText(TextMessage msg) {
        processChatMessage(msg);
    }

    @Override
    protected void processPoll(PollMessage msg) {
        processChatMessage(msg);
    }

    @Override
    protected void processOtherMessage(OtherMessage msg) {
        processChatMessage(msg);
    }

    // We need this to avoid sending bot commangs to the users.
    private void processChatMessage(BaseMessage msg) {
        final Update update = msg.getUpdate();

        final Message message = update.getMessage();
        final String chatId = Long.toString(update.getMessage().getChatId());

        if (Objects.equals(chatId, getData().getChatId())) {
            // Message in configured chat
            final Message original = message.getReplyToMessage();
            if (original != null && Objects.equals(original.getFrom().getId(), getBotUser().getId())) {
                // This is a reply to a bots message
                final User forwardFrom = original.getForwardFrom();
                final String originalChatId = forwardFrom != null
                        ? Long.toString(forwardFrom.getId())
                        : getMasterBot().getChatId(
                                Long.toString(original.getChatId()),
                        Integer.toString(original.getMessageId()));
                try {
                    if (originalChatId == null) {
                        sendMessage(chatId, "" +
                                "Не получилось найти оригинальное сообщение. " +
                                "Ответ не будет отправлен.");
                    } else {
                        copyMessage(originalChatId, message);
                    }
                } catch (TelegramApiException ex) {
                    onError(chatId, ex);
                }
            }
        }
    }

    public void secure(String userId, TelegramConsumer<Result> consumer) throws TelegramApiException {
        if (!isOwner(userId)) {
            consumer.consume(Result.NOT_OWNER);
            return;
        }

        getMasterBot().secure(this, consumer);
    }
}
