package org.butch.bots.feedback.slave.commands;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.feedback.Result;
import org.butch.bots.feedback.slave.BaseSlaveBot;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class BanCommand extends BaseOwnerCommand {
    public BanCommand(BaseSlaveBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "ban";
    }

    @Override
    public String getDescription() {
        return "Забанить пользователя";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " [причина]\n" +
                "Добавляет пользователя в список забаненных. " +
                "Все сообщения от пользователей в этом списке будут игнорироваться ботом.\n" +
                "Эту команду надо выполнить в ответ на сообщение пользователя, " +
                "которого необходимо забанить.\n" +
                "  <b>причина</b> - Причина бана. Необязательное поле.\n";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        final String userId = msg.getUserId();
        final String chatId = msg.getChatId();
        final String text = msg.getParams();

        // Attempt to ban a user.
        final Result result = bot.banUser(userId, msg.getMessage().getReplyToMessage(), text);
        if (result == Result.OK) {
            bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                    "Пользователь успешно забанен. "
            );
        } else {
            bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                    "Не удалось забанить пользователя: " + result.getDescription()
            );
        }
    }
}
