package org.butch.bots.feedback.master.commands;

import org.butch.bots.base.Utils;
import org.butch.bots.base.commands.Command;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.feedback.master.FeedbackMasterBot;
import org.butch.bots.feedback.slave.BaseSlaveBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

public class ListCommand extends Command<FeedbackMasterBot> {
    public ListCommand(FeedbackMasterBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "list";
    }

    @Override
    public String getDescription() {
        return "Напечатать список ботов";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + "\n" +
                "Печатает список ботов принадлежащих пользователю, " +
                "находящихся под управлением мастера.\n";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        final String userId = msg.getUserId();
        final String chatId = msg.getChatId();
        final List<BaseSlaveBot> list = bot.getList(userId);
        if (list.isEmpty()) {
            bot.sendMessage(chatId, "У вас нет ботов.");
        } else {
            final SendMessage sendMessage = new SendMessage();
            sendMessage.setChatId(chatId);
            sendMessage.setText("Список ботов:");
            final InlineKeyboardMarkup keyboard = new InlineKeyboardMarkup();
            keyboard.setKeyboard(new ArrayList<>());
            for (BaseSlaveBot slaveBot : list) {
                final User slaveUser = slaveBot.getBotUser();
                final String name = Utils.getUsername(slaveUser);
                final String url = Utils.getBotUrl(slaveUser);
                keyboard.getKeyboard().add(
                        new ArrayList<>() {{
                            add(InlineKeyboardButton.builder()
                                    .text(name)
                                    .url(url)
                                    .build()
                            );
                        }}
                );
            }
            sendMessage.setReplyMarkup(keyboard);
            bot.execute(sendMessage);
        }
    }
}
