package org.butch.bots.feedback.slave.commands;

import org.butch.bots.base.commands.Command;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.feedback.slave.BaseSlaveBot;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public abstract class BaseHelpCommand extends org.butch.bots.base.commands.BaseHelpCommand<BaseSlaveBot> {
    public static final int MAX_LENGTH = 2048;

    public BaseHelpCommand(BaseSlaveBot bot) {
        super(bot);
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        final boolean isOwner = bot.isOwner(msg.getUserId());
        final StringBuilder builder = new StringBuilder();
        builder.append(getHelp()).append("\n"); // Add this help first
        boolean firstCommand = true;
        for (Command<?> command : bot.getCommands()) {
            if (command == this) // Don't print this help like general command
                continue;
            if (!command.addHelp())
                continue;
            final String help;
            if (command instanceof OwnerCommand) {
                final OwnerCommand ownerCommand = (OwnerCommand) command;
                if (isOwner) {
                    help = ownerCommand.getHelp();
                } else {
                    help = null;
                }
            } else {
                help = command.getHelp();
            }
            if (help != null) {
                if (firstCommand) {
                    firstCommand = false;
                    builder.append("<u>Список поддерживаемых команд:</u>\n\n");
                }
                builder.append(help).append("\n");
                if (builder.length() > MAX_LENGTH) {
                    bot.sendHtml(msg.getChatId(), builder.toString());
                    builder.setLength(0);
                }
            }
        }
        if (builder.length() > 0) {
            bot.sendHtml(msg.getChatId(), builder.toString());
        }
    }
}
