package org.butch.bots.feedback.master;

import org.butch.bots.base.Bot;
import org.butch.bots.feedback.Constants;
import org.butch.bots.feedback.Result;
import org.butch.bots.feedback.TelegramConsumer;
import org.butch.bots.feedback.master.commands.HelpCommand;
import org.butch.bots.feedback.master.commands.ListCommand;
import org.butch.bots.feedback.master.commands.RegisterCommand;
import org.butch.bots.feedback.master.commands.StartCommand;
import org.butch.bots.feedback.master.repository.BanDto;
import org.butch.bots.feedback.master.repository.BotDto;
import org.butch.bots.feedback.master.repository.Storage;
import org.butch.bots.feedback.master.repository.StorageManager;
import org.butch.bots.feedback.slave.BaseSlaveBot;
import org.butch.bots.feedback.slave.basic.FeedbackSlaveBot;
import org.butch.bots.feedback.slave.secure.SecureFeedbackSlaveBot;
import org.butch.utils.TimeUtils;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.generics.BotSession;

import javax.crypto.SecretKey;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class FeedbackMasterBot extends Bot {
    private static final long CLEANUP_INTERVAL = TimeUtils.MS_IN_HOUR;

    private final TelegramBotsApi botsApi;
    private final String token;

    private final StorageManager storage;
    private final ConcurrentHashMap<Integer, BaseSlaveBot> bots = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<Integer, TelegramConsumer<Result>> botChangeConsumers = new ConcurrentHashMap<>();

    private long lastCleanUpTime = 0L;

    public FeedbackMasterBot(TelegramBotsApi botsApi, String token, Storage storage, SecretKey secretKey) {
        this.botsApi = botsApi;
        this.token = token;
        this.storage = new StorageManager(storage, secretKey);
        this.storage.init();

        addCommand(new HelpCommand(this));
        addCommand(new StartCommand(this));
        addCommand(new ListCommand(this));
        addCommand(new RegisterCommand(this));
    }

    @Override
    public String getBotUsername() {
        return "FeedbackMasterBot";
    }

    @Override
    public String getBotToken() {
        return token;
    }

    @Override
    public void onTimer(long time) {
        if (time - lastCleanUpTime >= CLEANUP_INTERVAL) {
            LOGGER.info("CleanUp");
            lastCleanUpTime = time;
            storage.deleteOldChatIds();
        }
    }

//    private static final HashSet<Integer> idsToRemove = new HashSet<>(List.of(new Integer[]{14}));

    @Override
    public void onRegister() {
        super.onRegister();
        synchronized (storage) {
            final List<BotDto> bots = storage.queryBots();
            final HashMap<String, BaseSlaveBot> uniqueBots = new HashMap<>();
            for (BotDto bot : bots) {
//                if (idsToRemove.contains(bot.getId())) {
//                    storage.deleteBot(bot);
//                    LOGGER.info("Bot deleted: " + bot);
//                    continue;
//                }
                LOGGER.info("Slave bot: " + bot.log());
                if (uniqueBots.containsKey(bot.getToken())) {
                    final BaseSlaveBot existing = uniqueBots.get(bot.getToken());
                    LOGGER.warn("Bot already registered: " + bot);
                    LOGGER.warn("Original bot: " + existing.getUserName() + " - " + existing.getData());
                    continue;
                }

                final BaseSlaveBot slaveBot = registerSlave(bot, true);
                if (slaveBot != null) {
                    uniqueBots.put(bot.getToken(), slaveBot); // TODO: check bots uniqueness
                }
            }

            final List<BanDto> allBans = storage.queryBans();
            for (BanDto banDto : allBans) {
                final BaseSlaveBot bot = this.bots.get(banDto.getBotId());
                if (bot == null) {
                    LOGGER.warn("Removing ban for deleted bot: " + banDto);
                    if (!storage.deleteBan(banDto)) {
                        LOGGER.error("Error removing ban: " + banDto);
                    }
                    continue;
                }
                bot.addBanFromDatabase(banDto);
            }
        }
    }

    private BaseSlaveBot registerSlave(BotDto bot, boolean save) {
        BotSession session = null;
        try {
            final BaseSlaveBot ret = bot.getIsSecure()
                    ? new SecureFeedbackSlaveBot(this, bot)
                    : new FeedbackSlaveBot(this, bot);
            session = botsApi.registerBot(ret);
            ret.setSession(session);
            final User user = ret.getMe();
            LOGGER.info("Adding slave bot: " + user.getUserName());
            ret.setUserName(user.getUserName());
            if (save) {
                bots.put(bot.getId(), ret);
            }
            return ret;
        } catch (Throwable ex) {
            LOGGER.error("Could not register bot " + bot + ": " + ex.getMessage());
            if (session != null) {
                session.stop();
            }
            // TODO: Consider removing from database
            return null;
        }
    }

    public List<BaseSlaveBot> getList(String ownerId) {
        return bots.values().stream()
                .filter(b -> Objects.equals(b.getData().getOwnerId(), ownerId))
                .collect(Collectors.toList());
    }

    public boolean register(String token, String userId, String chatId) {
        final BotDto bot = new BotDto();
        bot.setToken(token);
        bot.setOwnerId(userId);
        bot.setChatId(chatId);
        bot.setWelcome("Привет, " + Constants.USER_PLACEHOLDER + "!");
        bot.setIsSecure(false);
        final BaseSlaveBot slaveBot = registerSlave(bot, false);
        if (slaveBot != null) {
            synchronized (storage) {
                final int id = storage.insertBot(bot);
                bot.setId(id);
            }
            bots.put(bot.getId(), slaveBot);
        }
        return true;
    }

    public Result unregister(BaseSlaveBot slaveBot) {
        if (bots.remove(slaveBot.getData().getId()) == null) {
            LOGGER.warn("Bot not found in cache: " + slaveBot.getData());
            return Result.NOT_FOUND;
        }
        synchronized (storage) {
            if (!storage.deleteBot(slaveBot.getData())) {
                LOGGER.warn("Could not delete bot from database: " + slaveBot.getData());
                return Result.DB_ERROR;
            }
        }
        return Result.OK;
    }

    public Result updateBot(BotDto dto) {
        synchronized (storage) {
            if (!storage.updateBot(dto)) {
                LOGGER.warn("Could not update bot in database: " + dto);
                return Result.DB_ERROR;
            }
        }
        return Result.OK;
    }

    public void secure(FeedbackSlaveBot bot, TelegramConsumer<Result> consumer) throws TelegramApiException {
        final BaseSlaveBot baseBot = bots.remove(bot.getData().getId());
        if (!Objects.equals(bot, baseBot)) {
            LOGGER.error("Attempt to secure unknown bot: " + bot.getData());
            consumer.consume(Result.CRITICAL);
            return;
        }

        changeBot(bot, true, consumer);
    }

    public void unsecure(SecureFeedbackSlaveBot bot, TelegramConsumer<Result> consumer) throws TelegramApiException {
        final BaseSlaveBot baseBot = bots.remove(bot.getData().getId());
        if (!Objects.equals(bot, baseBot)) {
            LOGGER.error("Attempt to unsecure unknown bot: " + bot.getData());
            consumer.consume(Result.CRITICAL);
            return;
        }

        changeBot(bot, false, consumer);
    }

    public void changeBot(BaseSlaveBot bot, boolean toSecure, TelegramConsumer<Result> consumer) throws TelegramApiException {
        final TelegramConsumer<Result> stored = botChangeConsumers.compute(bot.getData().getId(), (key, val) -> {
            if (val == null)
                return consumer;
            return val;
        });
        if (stored != consumer) {
            consumer.consume(Result.IN_PROGRESS);
            return;
        }

        bot.getData().setIsSecure(toSecure);
        synchronized (storage) {
            if (!storage.updateBot(bot.getData())) {
                bot.getData().setIsSecure(!toSecure);
                bots.put(bot.getData().getId(), bot);
                botChangeConsumers.remove(bot.getData().getId());
                consumer.consume(Result.DB_ERROR);
            }
        }

        bot.stop();
    }

    public void restart(BaseSlaveBot bot) throws TelegramApiException {
        final TelegramConsumer<Result> consumer = botChangeConsumers.remove(bot.getData().getId());
        if (consumer == null)
            return;

        final BaseSlaveBot result = registerSlave(bot.getData(), true);
        if (result == null) {
            consumer.consume(Result.ERROR);
            return;
        }
        consumer.consume(Result.OK);
    }

    public boolean insertChatId(String messageChatId, String messageId, String chatId) {
        return storage.insertChatId(messageChatId, messageId, chatId);
    }

    public String getChatId(String messageChatId, String messageId) {
        return storage.getChatId(messageChatId, messageId);
    }

    public Result insertBan(BanDto banDto) {
        synchronized (storage) {
            final int id = storage.insertBan(banDto);
            if (id < 0) {
                LOGGER.warn("Could not insert ban to database: " + banDto);
                return Result.DB_ERROR;
            }
            banDto.setId(id);
        }
        return Result.OK;
    }

    public Result deleteBan(BanDto banDto) {
        synchronized (storage) {
            if (!storage.deleteBan(banDto)) {
                LOGGER.warn("Could not delete ban from database: " + banDto);
                return Result.DB_ERROR;
            }
        }
        return Result.OK;
    }
}
