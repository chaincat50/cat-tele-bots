package org.butch.bots.feedback.slave.basic;

import org.butch.bots.feedback.slave.BaseSlaveBot;
import org.butch.bots.feedback.slave.commands.BaseHelpCommand;

public class HelpCommand extends BaseHelpCommand {
    public HelpCommand(BaseSlaveBot bot) {
        super(bot);
    }

    @Override
    public String getHelp() {
        return "" +
                "<u><b>Описание работы бота.</b></u>\n" +
                "\n" +
                "Данный бот предназначен для обратной связи. " +
                "Напишите в него то, что вы хотели бы передать.\n";
    }
}
