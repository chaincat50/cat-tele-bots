package org.butch.bots.feedback.master.commands;

import org.butch.bots.base.commands.Command;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.feedback.master.FeedbackMasterBot;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class RegisterCommand extends Command<FeedbackMasterBot> {
    public RegisterCommand(FeedbackMasterBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "register";
    }

    @Override
    public String getDescription() {
        return "<токен> Зарегистрировать бота";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + " &lt;токен&gt;\n" +
                "Отдаёт нового бота в управление мастера.\n" +
                "  <b>токен</b> - токен бота, предназначенный для управления им.\n";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        final String token = msg.getParams().trim();
        final String userId = msg.getUserId();
        final String chatId = msg.getChatId();
        if (bot.register(token, userId, chatId)) {
            bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                    "Бот успешно зарегистрирован. " +
                    "Теперь вы можете посылать в него команды.\n" +
                    "Обратите внимание: никто другой управлять ботом не сможет. " +
                    "Передача бота через @BotFather не меняет владельца. " +
                    "Если вы захотите поменять владельца - вам надо будет резрегистрировать бота, " +
                    "а другому человеку - зарегистрировать заново. " +
                    "При этом, все настройки будут потеряны."
            );
        } else {
            bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                    "Не удалось зарегистрировать бота. " +
                    "Проверьте правильность токена: '" + token + "'"
            );
        }
    }
}
