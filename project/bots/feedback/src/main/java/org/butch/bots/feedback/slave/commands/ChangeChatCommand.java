package org.butch.bots.feedback.slave.commands;

import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.feedback.Result;
import org.butch.bots.feedback.slave.BaseSlaveBot;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class ChangeChatCommand extends BaseOwnerCommand {
    public ChangeChatCommand(BaseSlaveBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "chat";
    }

    @Override
    public String getDescription() {
        return "Поменять чат для общения";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + "\n" +
                "Устанавливает текущий чат в качестве чата для общения. " +
                "Чат для общения может быть только один. " +
                "Это может быть либо обычный чат, либо личка владельца бота.\n";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        final String userId = msg.getUserId();
        final String chatId = msg.getChatId();
        final Result result = bot.changeChat(userId, chatId);
        if (result == Result.OK) {
            bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                    "Чат для общения успешно изменён на текущий."
            );
        } else {
            bot.sendReply(chatId, msg.getMessage().getMessageId(), "" +
                    "Не удалось изменить чат для общения. " + result.getDescription()
            );
        }
    }
}
