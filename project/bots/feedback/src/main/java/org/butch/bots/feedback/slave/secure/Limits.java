package org.butch.bots.feedback.slave.secure;

public final class Limits {
    public static final int MAX_TEXT_LENGTH = 3500;
    public static final int MAX_PHOTO_CAPTION_LENGTH = 700;
    public static final int MIN_SALT_LENGTH = 50;
    public static final int MAX_SALT_LENGTH = 100;
}
