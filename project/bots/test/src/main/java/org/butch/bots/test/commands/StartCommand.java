package org.butch.bots.test.commands;

import org.butch.bots.base.commands.BaseStartCommand2;
import org.butch.bots.test.TestBot;

public class StartCommand extends BaseStartCommand2<TestBot> {
    public StartCommand(TestBot bot) {
        super(bot);
    }
}
