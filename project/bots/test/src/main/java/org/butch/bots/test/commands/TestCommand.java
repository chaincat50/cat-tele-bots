package org.butch.bots.test.commands;

import org.butch.bots.base.commands.Command;
import org.butch.bots.base.helpers.KeyboardHelper;
import org.butch.bots.base.helpers.send.DefaultSendMessageHelper;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.test.TestBot;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;

public class TestCommand extends Command<TestBot> {
    public TestCommand(TestBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "test";
    }

    @Override
    public String getDescription() {
        return "Тест";
    }

    @Override
    public String getHelp() {
        return "<b>/test</b> - Тестовая команда.";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        final KeyboardHelper keyboardHelper = new KeyboardHelper(bot);
        final InlineKeyboardMarkup keyboard = keyboardHelper.inline()
                .button("Button", "1")
                .build();

        final DefaultSendMessageHelper sendHelper = new DefaultSendMessageHelper(bot);
        sendHelper.send(msg.getChatId(), "Test", keyboard);
    }
}
