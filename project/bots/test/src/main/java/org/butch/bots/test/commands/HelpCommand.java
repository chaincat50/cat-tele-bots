package org.butch.bots.test.commands;

import org.butch.bots.base.commands.BaseHelpCommand2;
import org.butch.bots.test.TestBot;

public class HelpCommand extends BaseHelpCommand2<TestBot> {
    public HelpCommand(TestBot bot) {
        super(bot);
    }

    @Override
    public String getHelp() {
        return "" +
                "<u><b>Тестовый бот.</b></u>\n";
    }
}
