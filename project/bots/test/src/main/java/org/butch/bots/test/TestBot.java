package org.butch.bots.test;

import org.butch.bots.base.Bot2;
import org.butch.bots.base.error.writer.ReplyErrorWriter;
import org.butch.bots.base.filter.callback.CallbackProcessor;
import org.butch.bots.base.filter.commands.CommandsProcessor;
import org.butch.bots.base.filter.commands.ReplyUnknownCommand;
import org.butch.bots.base.helpers.send.DefaultSendMessageHelper;
import org.butch.bots.base.messages.Callback;
import org.butch.bots.test.commands.HelpCommand;
import org.butch.bots.test.commands.SendCommand;
import org.butch.bots.test.commands.StartCommand;
import org.butch.bots.test.commands.TestCommand;

public class TestBot extends Bot2 {
    private final String token;

    public TestBot(String token) {
        this.token = token;

        setDefaultErrorWriter(new ReplyErrorWriter(this));
        addFilter(
                new CommandsProcessor()
                        .addHelp(new HelpCommand(this))
                        .addCommand(new StartCommand(this))
                        .addCommand(new TestCommand(this))
                        .addCommand(new SendCommand(this))
                        .onUnknownCommand(new ReplyUnknownCommand(new DefaultSendMessageHelper(this)))
        );
        addFilter(new CallbackProcessor(this::processCallback));
    }

    @Override
    public String getBotUsername() {
        return "TestBot";
    }

    @Override
    public String getBotToken() {
        return token;
    }

    private void processCallback(Callback callback) {
        if (callback.getData() == null) {
            LOGGER.warn("Callback without data called: " + callback.getQuery());
            return;
        }
        LOGGER.debug(callback.getData());
    }
}
