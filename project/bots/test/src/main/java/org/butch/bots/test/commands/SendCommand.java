package org.butch.bots.test.commands;

import org.butch.bots.base.Emoji;
import org.butch.bots.base.commands.Command;
import org.butch.bots.base.helpers.ChatHelper;
import org.butch.bots.base.helpers.send.DefaultSendMessageHelper;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.test.TestBot;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class SendCommand extends Command<TestBot> {
    private final ChatHelper chatHelper;
    private final DefaultSendMessageHelper sendHelper;

    public SendCommand(TestBot bot) {
        super(bot);
        chatHelper = new ChatHelper(bot);
        sendHelper = new DefaultSendMessageHelper(bot);
    }

    @Override
    public String getCommand() {
        return "send";
    }

    @Override
    public String getDescription() {
        return "Тест";
    }

    @Override
    public String getHelp() {
        return "<b>/send</b> - Тестовая команда.";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        final String s = Emoji.Uncategorized.SMALL_WHITE_SQUARE;
        final String p = Emoji.PersonActivity.PERSON_STANDING;
        sendHelper.send(msg.getChatId(), "" +
                s + s + s + s + s + "\n" +
                s + s + s + s + s + "\n" +
                s + s + p + s + s + "\n" +
                s + s + s + s + s + "\n" +
                s + s + s + s + s + "\n"
        );
    }
}
