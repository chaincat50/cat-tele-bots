package org.butch.bots.id;

import org.butch.bots.base.Bot;
import org.butch.bots.id.commands.HelpCommand;
import org.butch.bots.id.commands.MyIdCommand;
import org.butch.bots.id.commands.StartCommand;
import org.telegram.telegrambots.meta.api.methods.groupadministration.GetChatAdministrators;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.chatmember.ChatMember;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;

public class IdBot extends Bot {
    private final String token;

    public IdBot(String token) {
        this.token = token;
        addCommand(new HelpCommand(this));
        addCommand(new StartCommand(this));
        addCommand(new MyIdCommand(this));
    }

    @Override
    public String getBotUsername() {
        return "IdBot";
    }

    @Override
    public String getBotToken() {
        return token;
    }

    @Override
    protected void processNewGroupUser(String chatId, User user) {
        try {
            final ArrayList<ChatMember> admins = execute(GetChatAdministrators.builder().chatId(chatId).build());

        } catch (TelegramApiException e) {
            onError(chatId, e);
        }
    }

    @Override
    protected void processLeftGroupUser(String chatId, User user) {
    }
}
