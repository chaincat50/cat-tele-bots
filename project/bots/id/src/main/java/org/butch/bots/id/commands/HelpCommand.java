package org.butch.bots.id.commands;

import org.butch.bots.base.commands.BaseHelpCommand;
import org.butch.bots.id.IdBot;

public class HelpCommand extends BaseHelpCommand<IdBot> {
    public HelpCommand(IdBot bot) {
        super(bot);
    }

    @Override
    public String getHelp() {
        return "" +
                "<u><b>Описание работы бота.</b></u>\n" +
                "\n" +
                "<u>Список поддерживаемых команд:</u>\n";
    }
}
