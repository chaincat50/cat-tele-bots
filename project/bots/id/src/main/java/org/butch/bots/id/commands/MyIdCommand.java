package org.butch.bots.id.commands;

import org.butch.bots.base.commands.Command;
import org.butch.bots.base.messages.CommandMessage;
import org.butch.bots.id.IdBot;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Objects;

public class MyIdCommand extends Command<IdBot> {
    public MyIdCommand(IdBot bot) {
        super(bot);
    }

    @Override
    public String getCommand() {
        return "myid";
    }

    @Override
    public String getDescription() {
        return "Печатает ID пользователя";
    }

    @Override
    public String getHelp() {
        return "" +
                COMMAND_PREFIX + getCommand() + "\n" +
                "\n" +
                "Печатает ID пользователя. Разрешено использовать только в личке бота.\n";
    }

    @Override
    public void process(CommandMessage msg) throws TelegramApiException {
        if (!Objects.equals(msg.getChatId(), msg.getUserId())) {
            bot.sendMessage(msg.getChatId(), "chatId: " + msg.getChatId());
        } else {
            bot.sendMessage(msg.getChatId(), "userId: " + msg.getUserId());
        }
    }
}
