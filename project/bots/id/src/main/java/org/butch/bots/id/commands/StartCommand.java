package org.butch.bots.id.commands;

import org.butch.bots.base.commands.BaseStartCommand;
import org.butch.bots.id.IdBot;

public class StartCommand extends BaseStartCommand<IdBot> {
    public StartCommand(IdBot bot) {
        super(bot);
    }
}
